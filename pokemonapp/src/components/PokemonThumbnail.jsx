import React, { useState } from "react";
import Description from "./Description";
import TransitionModal from "./Modal";
import {
  Button,
  Card,
  CardActionArea,
  CardMedia,
  Typography,
  CardContent,
} from "@mui/material";

const PokemonThumbnail = ({
  id,
  name,
  image,
  type,
  height,
  weight,
  stat1,
  stat2,
  stat3,
  stat4,
  stat5,
  stat6,
  bs1,
  bs2,
  bs3,
  bs4,
  bs5,
  bs6,
}) => {
  const style = `thumb-container ${type}`;
  //   const [show, setShow] = useState(false);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <React.Fragment>
      <Card sx={{ maxWidth: 345 }}>
        <CardActionArea>
          <CardMedia component="img" height="140" image={image} alt={name} />
          <CardContent>
            <Typography variant="h5" component="div">
              {`#0${id} ${name.toUpperCase()}`}
            </Typography>
            <Typography gutterBottom variant="h6" component="div">
              Type : {type}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Lizards are a widespread group of squamate reptiles, with over
              6,000 species, ranging across all continents except Antarctica
            </Typography>
            <Button variant="contained" sx={{marginTop: "10px"}} onClick={handleOpen}>
              Know More
            </Button>
            <TransitionModal
              handleClose={handleClose}
              image={image}
              open={open}
              id={id}
              name={name}
              heightpok={height}
              weightpok={weight}
              pokstat1={stat1}
              pokstat2={stat2}
              pokstat3={stat3}
              pokstat4={stat4}
              pokstat5={stat5}
              pokstat6={stat6}
              posbs1={bs1}
              posbs2={bs2}
              posbs3={bs3}
              posbs4={bs4}
              posbs5={bs5}
              posbs6={bs6}
            />
          </CardContent>
        </CardActionArea>
      </Card>
    </React.Fragment>
  );
};

export default PokemonThumbnail;
