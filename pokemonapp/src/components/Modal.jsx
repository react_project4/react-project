import React from "react";
import { Modal, Fade, Box, Typography, Backdrop } from "@mui/material";
import Description from "./Description";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const TransitionModal = ({
  open,
  handleClose,
  image,
  name,
  heightpok,
  weightpok,
  pokstat1,
  pokstat2,
  pokstat3,
  pokstat4,
  pokstat5,
  pokstat6,
  posbs1,
  posbs2,
  posbs3,
  posbs4,
  posbs5,
  posbs6,
}) => {
  return (
    <React.Fragment>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography
              id="transition-modal-title"
              variant="h5"
              component="div"
            >
              {name}
            </Typography>
            <img src={image} alt={name} />
            <Typography id="transition-modal-description">
              <Description
                heightpok={heightpok}
                weightpok={weightpok}
                pokstat1={pokstat1}
                pokstat2={pokstat2}
                pokstat3={pokstat3}
                pokstat4={pokstat4}
                pokstat5={pokstat5}
                pokstat6={pokstat6}
                posbs1={posbs1}
                posbs2={posbs2}
                posbs3={posbs3}
                posbs4={posbs4}
                posbs5={posbs5}
                posbs6={posbs6}
              />
            </Typography>
          </Box>
        </Fade>
      </Modal>
    </React.Fragment>
  );
};

export default TransitionModal;
