import React from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  TableBody,
  Typography,
  Box,
} from "@mui/material";

const Description = ({
  heightpok,
  weightpok,
  pokstat1,
  pokstat2,
  pokstat3,
  pokstat4,
  pokstat5,
  pokstat6,
  posbs1,
  posbs2,
  posbs3,
  posbs4,
  posbs5,
  posbs6,
}) => {
  const StatsObj = [
    { stats: pokstat1, base: posbs1 },
    { stats: pokstat2, base: posbs2 },
    { stats: pokstat3, base: posbs3 },
    { stats: pokstat4, base: posbs4 },
    { stats: pokstat5, base: posbs5 },
    { stats: pokstat6, base: posbs6 },
  ];

  return (
    <React.Fragment>
      <Box style={{fontWeight: 700}}>
        <Typography>{`Height: ${heightpok * 10} cm, Weight: ${weightpok * 0.1} kg`}</Typography>
      </Box>
      <TableContainer component={Paper}>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Stats</TableCell>
              <TableCell align="right">Base Stats</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {StatsObj.map((elem, index) => (
              <TableRow
                key={index}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {elem.stats}
                </TableCell>
                <TableCell align="right">{elem.base}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
};

export default Description;
