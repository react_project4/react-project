import React, {useState} from 'react'

const API_Service_Call = () => {
    const [allPokemons,setAllPokemons] = useState([]);
    const [loadPoke,setLoadPoke] = useState('https://pokeapi.co/api/v2/pokemon?limit=20');

    const getAllPokemons = async () => {
        const res = await fetch(loadPoke);
        const data = await res.json()
        setLoadPoke(data)
    }

    console.log(loadPoke)

  return (
    <React.Fragment></React.Fragment>
  )
}

export default API_Service_Call